/* FILE: func_xshape.cpp ----------------------------------------------- */
/* VERSION 0.3
 *
 *   My attempt to implement function that represents x-shaped bulges of
 *   edge-on galaxies.
 *   
 *   BASIC IDEA:
 *      Setup() is called as the first part of invoking the function;
 *      it pre-computes various things that don't depend on x and y.
 *      GetValue() then completes the calculation, using the actual value
 *      of x and y, and returns the result.
 *      So for an image, we expect the user to call Setup() once at
 *      the start, then loop through the pixels of the image, calling
 *      GetValue() to compute the function results for each pixel coordinate
 *      (x,y).
 *
 *   NOTE: Currently, we assume input PA is in *degrees* [and then we
 * convert it to radians] relative to +x axis.
 *
 *   MODIFICATION HISTORY:
 *     [v0.4]  20--26 Mar 2010: Preliminary support for pixel subsampling.
 *     [v0.3]: 21 Jan 2010: Modified to treat x0,y0 as separate inputs.
 *     [v0.2]: 28 Nov 2009: Updated to new FunctionObject interface.
 *     [v0.1]: 19 Nov 2009: Created (as modification of func_exp.cpp.
 */

// Copyright 2010, 2011, 2012, 2013 by Peter Erwin.
// 
// This file is part of Imfit.
// 
// Imfit is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your
// option) any later version.
// 
// Imfit is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
// 
// You should have received a copy of the GNU General Public License along
// with Imfit.  If not, see <http://www.gnu.org/licenses/>.



/* ------------------------ Include Files (Header Files )--------------- */
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <string>
#include <algorithm>

#include "func_xshape.h"

using namespace std;


/* ---------------- Definitions ---------------------------------------- */
const int  N_PARAMS = 9;
const char  PARAM_LABELS[][20] = {"PA", "ell", "I0", "rout", "alpha", "beta", "z0", "ax", "ay"};
const char  FUNCTION_NAME[] = "Xshape function";
const char SHORT_FUNCTION_NAME[] = "Xshape";
const double  DEG2RAD = 0.017453292519943295;
const double pi = 3.14159265;
const int  SUBSAMPLE_R = 10;

const char Xshape::className[] = "Xshape";

const double  A0_M03 = 0.01945;
const double  A1_M03 = -0.8902;
const double  A2_M03 = 10.95;
const double  A3_M03 = -19.67;
const double  A4_M03 = 13.43;


/* ---------------- CONSTRUCTOR ---------------------------------------- */

Xshape::Xshape( )
{
  string  paramName;
  
  nParams = N_PARAMS;
  functionName = FUNCTION_NAME;
  shortFunctionName = className;
  // Set up the vector of parameter labels
  for (int i = 0; i < nParams; i++) {
    paramName = PARAM_LABELS[i];
    parameterLabels.push_back(paramName);
  }
  
  doSubsampling = true;
}


/* ---------------- PUBLIC METHOD: Setup ------------------------------- */

void Xshape::Setup( double params[], int offsetIndex, double xc, double yc )
{
  /* x0 =  params[0 + offsetIndex];   -- center of component (pixels, x)
 * y0 =  params[1 + offsetIndex];   -- center of component (pixels, y)
 * PA =  params[2 + offsetIndex];   -- PA of component, rel. to +x axis
 * ell = params[3 + offsetIndex];   -- ellipticity
 * I0 = params[4 + offsetIndex];   -- cental brightness
 * rout = params[5 + offsetIndex];   -- Ferrer out truncation radius
 * alpha = params[6 + offsetIndex];   -- Ferrer alpha parameter
 * beta = params[7 + offsetIndex];   -- Ferrer beta parameter
 * z0 = params[8 + offsetIndex];   -- Vertical scalelength
 * ax = params[9 + offsenIndex];   -- horisonthal Fourier modification
 * ay = params[10 + offsenIndex];   -- vertical Fourier modification */
  x0 = xc;
  y0 = yc;
  PA = params[0 + offsetIndex];
  ell = params[1 + offsetIndex];
  I0 = params[2 + offsetIndex];
  rout = params[3 + offsetIndex];
  alpha = params[4 + offsetIndex];
  beta = params[5 + offsetIndex];
  z0 = params[6 + offsetIndex];
  ax = params[7 + offsetIndex];
  ay = params[8 + offsetIndex];

  // pre-compute useful things for this round of invoking the function
  q = 1.0 - ell;
  // convert PA to +x-axis reference and then to radians
  PA_rad = (PA + 90.0) * DEG2RAD;
  cosPA = cos(PA_rad);
  sinPA = sin(PA_rad);
}


/* ---------------- PRIVATE METHOD: CalculateIntensity ----------------- */
// This function calculates the intensity for a Sersic function at radius r,
// with the various parameters and derived values (n, b_n, r_e, etc.)
// pre-calculated by Setup().

double Xshape::CalculateIntensity( double R, double z )
{
  if (R > rout)
    return 0.0;
  double  intensity;
  if ((ax == 0.0) && (ay == 0.0)){
    // no Fourier modification
    intensity = I0 * pow((1.0 - pow(R/rout, 2.0-beta)), alpha) * pow(1.0/cosh(z/z0), 2.0);
    return intensity;
  }
  // if user wants to perform Fourier modification of the profile.
  // note, that small r stands for galactocentric distance.
  // small r and phi compose together the polar coordinate system,
  // whereas capital R and z compose cartesian system
  double r, phi;
  r = hypot(R, z);
  phi = atan2(z, R);
  //printf("%1.2f   %1.2f   %1.2f\n", xp, yp_scaled, phi);
  // First step: modifying of the galactocentric distance r
  r = r * (1.0 + ax*fabs(cos(phi+pi/4.0)) + ay*fabs(cos(phi+3*pi/4.0))); // * (1.0 + );
  // second step: compute new cartesian coordinates by modifyed polar ones
  // printf("-----\n");
  // printf("%1.2f   %1.2f\n", R, z);
  R = r * cos(phi);
  z = r * sin(phi);
  // printf("%1.2f   %1.2f\n", R, z);
  // finally we can compute modyfied intencity:
  if (R > rout)
    return 0.0;
  intensity = I0 * pow((1.0 - pow(R/rout, 2.0-beta)), alpha) * pow(1.0/cosh(z/z0), 2.0);
  return intensity;
  
}


/* ---------------- PUBLIC METHOD: GetValue ---------------------------- */
// This function calculates and returns the intensity value for a pixel with
// coordinates (x,y), including pixel subsampling if necessary (and if subsampling
// is turned on). The CalculateIntensity() function is called for the actual
// intensity calculation.

double Xshape::GetValue( double x, double y )
{
  double  x_diff = x - x0;
  double  y_diff = y - y0;
  double  xp, yp, yp_scaled, totalIntensity, z, R, r, phi;
  int  nSubsamples;
  
  // Calculate x,y in component reference frame, and scale y by 1/axis_ratio
  xp = x_diff*cosPA + y_diff*sinPA;
  yp = (-x_diff*sinPA + y_diff*cosPA);
  yp_scaled = yp / q;

  // Calculate R,z (= x,y in component reference frame)
  R = fabs(x_diff*cosPA + y_diff*sinPA);    // "R" is x in the component reference frame
  z = fabs(-x_diff*sinPA + y_diff*cosPA);   // "z" is y in the component reference frame
  
  nSubsamples = CalculateSubsamples(R, z);
  if (nSubsamples > 1) {
    // Do subsampling
    // start in center of leftmost/bottommost sub-picel
    double deltaSubpix = 1.0 / nSubsamples;
    double x_sub_start = x - 0.5 + 0.5*deltaSubpix;
    double y_sub_start = y - 0.5 + 0.5*deltaSubpix;
    double theSum = 0.0;
    for (int ii = 0; ii < nSubsamples; ii++) {
      double x_ii = x_sub_start + ii*deltaSubpix;
      for (int jj = 0; jj < nSubsamples; jj++) {
        double y_ii = y_sub_start + jj*deltaSubpix;
        x_diff = x_ii - x0;
        y_diff = y_ii - y0;
        R = fabs(x_diff*cosPA + y_diff*sinPA);
        z = fabs(-x_diff*sinPA + y_diff*cosPA);
        theSum += CalculateIntensity(R, z);
      }
    }
    totalIntensity = theSum / (nSubsamples*nSubsamples);
  }
  else
    totalIntensity = CalculateIntensity(R, z);

  return totalIntensity;
}


/* ---------------- PROTECTED METHOD: CalculateSubsamples ------------------------- */
// Function which determines the number of pixel subdivisions for sub-pixel integration,
// given that the current pixel is a distance of r away from the center of the
// Sersic function.
// This function returns the number of x and y subdivisions; the total number of subpixels
// will then be the return value *squared*.
int Xshape::CalculateSubsamples( double R, double z )
{
  int  nSamples = 1;
  int  nSr, nSz;
  double  R_abs = fabs(R);
  double  z_abs = fabs(z);
  
  // based on standard exponential-function subsampling
  if ( (doSubsampling) && ((R_abs < 10.0) || (z_abs < 10.0)) ) {
    if ( ((rout <= 1.0) && (R_abs <= 1.0)) || ((z0 <= 1.0) && (z_abs <= 1.0)) ) {
      nSr = min(100, (int)(2 * SUBSAMPLE_R / rout));
      nSz = min(100, (int)(2 * SUBSAMPLE_R / z0));
      nSamples = max(nSr, nSz);
    }
    else {
      if ((R_abs <= 3.0) || (z_abs <= 3.0))
        nSamples = 2 * SUBSAMPLE_R;
      else {
        nSr = min(100, (int)(2 * SUBSAMPLE_R / R_abs));
        nSz = min(100, (int)(2 * SUBSAMPLE_R / z_abs));
        nSamples = max(nSr, nSz);
      }
    }
  }
  return nSamples;
}


/* END OF FILE: func_sersic.cpp ---------------------------------------- */
