/*   Class interface definition for func_sersic.cpp
 *   VERSION 0.3
 *
 *   A class derived from FunctionObject (function_object.h),
 * which produces the luminosity as a function of radius for an elliptical
 * Sersic.
 *
 * PARAMETERS:
 * x0 =   params[0 + offsetIndex];   -- center of component (pixels, x)
 * y0 =   params[1 + offsetIndex];   -- center of component (pixels, y)
 * ell =  params[2 + offsetIndex];   -- ellipticity
 * I_e =  params[3 + offsetIndex ];  -- half-light-radius intensity (ADU)
 * n1 =   params[4 + offsetIndex ];  -- Sersic index along the lobe
 * r_e1 = params[5 + offsetIndex ];  -- half-light radius (pixels) along the lobe
 * n2 =   params[6 + offsetIndex ];  -- Sersic index across the lobe
 * r_e2 = params[7 + offsetIndex ];  -- half-light radius across the lobe
 * asymm = params[8 + offsetIndex ]; -- asymmetry between x and y distances
 * bend = params[9 + bend ]; -- bending of the x-structure
 */


// CLASS Sersic:

#include "function_object.h"

//#define CLASS_SHORT_NAME  "Sersic"


class Sharpx : public FunctionObject
{
  // the following static constant will be defined/initialized in the .cpp file
  static const char  className[];

  public:
    // Constructors:
    Sharpx( );
    // redefined method/member function:
    void  Setup( double params[], int offsetIndex, double xc, double yc );
    double  GetValue( double x, double y );
    // No destructor for now

    // class method for returning official short name of class
    static void GetClassShortName( string& classname ) { classname = className; };


  private:
    double  x0, y0, PA, ell, I_e, n1, r_e1, n2, r_e2, asymm, bend;   // parameters
    double  n1Squared, n2Squared, bn1, invn1, bn2, invn2;
  double  q;   // other useful (shape-related) quantities
};
