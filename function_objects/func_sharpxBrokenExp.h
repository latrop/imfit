/*   Class interface definition for func_sersic.cpp
 *   VERSION 0.3
 *
 *   A class derived from FunctionObject (function_object.h),
 * which produces the luminosity as a function of radius for an elliptical
 * Sersic.
 *
 * PARAMETERS:
 * x0 =   params[0 + offsetIndex];   -- center of component (pixels, x)
 * y0 =   params[1 + offsetIndex];   -- center of component (pixels, y)
 * ell =  params[2 + offsetIndex];   -- ellipticity
 * I0 =  params[3 + offsetIndex ];  -- central intensity intensity (ADU)
 * h_inner = params[4 + offsetIndex ]; -- inner exponential scale length (iside of rbreak)
 * h_outer = params[5 + offsetIndex ]; -- outer exponential scale length (outside of rbreak)
 * r_break = params[6 + offsetIndex ]; -- breaking radius (where the transition for h_inner to h_outer occures)
 * n_across =   params[7 + offsetIndex ];  -- Sersic index across the lobe
 * r_e_across = params[8 + offsetIndex ];  -- half-light radius across the lobe
 * asymm = params[9 + offsetIndex ]; -- asymmetry between x and y distances
 */


// CLASS Sersic:

#include "function_object.h"

//#define CLASS_SHORT_NAME  "Sersic"


class SharpxBrokenExp : public FunctionObject
{
  // the following static constant will be defined/initialized in the .cpp file
  static const char  className[];

  public:
    // Constructors:
    SharpxBrokenExp( );
    // redefined method/member function:
    void  Setup( double params[], int offsetIndex, double xc, double yc );
    double  GetValue( double x, double y );
    // No destructor for now

    // class method for returning official short name of class
    static void GetClassShortName( string& classname ) { classname = className; };


  private:
  double  x0, y0, ell, I0, h_inner, h_outer, r_break, n_across, r_e_across, asymm;
  double  n_across_Squared, bn2, invn2, I02;
  double  q;   // other useful (shape-related) quantities
};
