/*   Class interface definition for func_gaussianring3donearm.cpp
 *   VERSION 0.1
 *
 *   A class derived from FunctionObject (function_object.h), which produces projected
 * surface intensity for an elliptical 3D ring (luminosity density = Gaussian centered at 
 * r0 with width sigma and vertical exponential with scale heigh h_z), with major-axis
 * position angle PA_ring relative to the line of nodes; the overall orientation is
 * defined by the PA of the line of nodes (PA) and the inclination (inc).
 *
 * PARAMETERS:
 * x0 = xc;   -- center of component (pixels, x)
 * y0 = yc;   -- center of component (pixels, y)
 * PA = params[0 + offsetIndex];   -- PA of component line-of-nodes, rel. to +x axis
 * inclination = params[1 + offsetIndex];  -- inclination to line of sight (i=0 for face-on)
 * spiralPA = params[2 + offsetIndex];  -- PA of ring major axis, relative to line of nodes
 * ell = params[3 + offsetIndex];  -- ellipticity of ring
 * J_0 = params[4 + offsetIndex ];  -- central luminosity density (ADU)
 * pitch = params[5 + offsetIndex ];   -- pitch angle at the centre
 * dPitch = params[6 + offsetIndex ]; -- linear pitch angle variation (pitch(R) = pitch + R*dPitch)
 * sigma = params[7 + offsetIndex ];   -- width of spirals
 * sigmaVar = params[8 + offsetIndex ];  -- variation of spiral width
 * h_z = params[9 + offsetIndex ];  -- vertical scale
 * h = params[10 + offsetIndex ];  -- radial scale
 * rIn = params[11 + offsetIndex ]; -- inner radius of the arm
 * rOut = params[12 + offsetIndex ]; -- outer radius of the arm
 * soft = params[13 + offsetIndex ]; -- softening parameter of the arm fade away outside of rin-rout range
 */


// CLASS GaussianRing3DOneArm:

#include <string>
#include "gsl/gsl_integration.h"
#include "function_object.h"

using namespace std;

//#define CLASS_SHORT_NAME  "GaussianRing3DOneArm"


class Spirals3DOneArm : public FunctionObject
{
  // the following static constant will be defined/initialized in the .cpp file
  static const char  className[];
  
  public:
    // Constructor
    Spirals3DOneArm( );
    // redefined method/member function:
    void  Setup( double params[], int offsetIndex, double xc, double yc );
    double  GetValue( double x, double y );
    // No destructor for now

    // class method for returning official short name of class
    static void GetClassShortName( string& classname ) { classname = className; };


  private:
    double  x0, y0, PA, inclination, spirPA, ell, J_0, pitch, dPitch, sigma, sigmaVar, h_z, h, rIn, rOut, soft;   // parameters
    double  cosPA, sinPA, cosInc, sinInc, spirPA_rad;   // other useful quantities
    double  cosSpirPA, sinSpirPA, q, twosigma_squared;
    gsl_function  F;
};

