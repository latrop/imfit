/* FILE: func_gaussianring3dvarpitch.cpp -------------------------------------- */
/* VERSION 0.2
 *
 *   Experimental function object class for an elliptical 3D ring (luminosity
 * density = Gaussian centered at radius a_ring along major axis, with width sigma 
 * and vertical exponential with scale heigh h_z); ring has intrinsic (in-plane) 
 * ellipticity ell and position angle PA_ring w.r.t. to line of nodes; system is 
 * seem with line of nodes at angle PA (w.r.t. image +y axis) and inclination inc.
 *   
 *   BASIC IDEA:
 *      Setup() is called as the first part of invoking the function;
 *      it pre-computes various things that don't depend on x and y.
 *      GetValue() then completes the calculation, using the actual value
 *      of x and y, and returns the result.
 *      So for an image, we expect the user to call Setup() once at
 *      the start, then loop through the pixels of the image, calling
 *      GetValue() to compute the function results for each pixel coordinate
 *      (x,y).
 *
 *   NOTE: Currently, we assume input PA is in *degrees* [and then we
 * convert it to radians] relative to +x axis.
 *
 *   MODIFICATION HISTORY:
 *     [v0.2]: 21 Oct 2012: Modified from circular to elliptical ring shape.
 *     [v0.1]: 24 Aug 2012: Created (as modification of func_exp3d.cpp).
 */





/* ------------------------ Include Files (Header Files )--------------- */
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <string>
#include <gsl/gsl_errno.h>

#include "func_spirals3dvarpitch.h"
#include "integrator.h"


using namespace std;

/* ---------------- Definitions ---------------------------------------- */
const int   N_PARAMS = 11;
const char  PARAM_LABELS[][20] = {"PA", "inc", "PA_spiral", "ell", "J_0", "pitch", "dPitch", "sigma", "sigmaVar", "h_z", "h"};
const char  FUNCTION_NAME[] = "Spirals3DvarPitch function";
const double  DEG2RAD = 0.017453292519943295;
const int  SUBSAMPLE_R = 10;
const double  COSH_LIMIT = 100.0;
const double  INTEGRATION_MULTIPLIER = 5;

const char Spirals3DvarPitch::className[] = "Spirals3DvarPitch";


/* ---------------- Local Functions ------------------------------------ */

double LuminosityDensitySpiralsVP( double s, void *params );




/* ---------------- CONSTRUCTOR ---------------------------------------- */

Spirals3DvarPitch::Spirals3DvarPitch( )
{
  string  paramName;
  
  nParams = N_PARAMS;
  functionName = FUNCTION_NAME;
  shortFunctionName = className;

  // Set up the vector of parameter labels
  for (int i = 0; i < nParams; i++) {
    paramName = PARAM_LABELS[i];
    parameterLabels.push_back(paramName);
  }

  // Stuff related to GSL integration
  gsl_set_error_handler_off();
  F.function = LuminosityDensitySpiralsVP;
  
  doSubsampling = false;
}


/* ---------------- PUBLIC METHOD: Setup ------------------------------- */

void Spirals3DvarPitch::Setup( double params[], int offsetIndex, double xc, double yc )
{
  double  PA_rad, inc_rad;
  
  x0 = xc;
  y0 = yc;
  PA = params[0 + offsetIndex];
  inclination = params[1 + offsetIndex];
  spirPA = params[2 + offsetIndex];
  ell = params[3 + offsetIndex];
  J_0 = params[4 + offsetIndex ];
  pitch = params[5 + offsetIndex ];
  dPitch = params[6 + offsetIndex ];
  sigma = params[7 + offsetIndex ];
  sigmaVar = params[8 + offsetIndex ];
  h_z = params[9 + offsetIndex ];
  h = params[10 + offsetIndex ];

  // pre-compute useful things for this round of invoking the function
  q = 1.0 - ell;
  // convert PA to +x-axis reference
  PA_rad = PA * DEG2RAD;
  cosPA = cos(PA_rad);
  sinPA = sin(PA_rad);
  
  // ring PA rotations are computed relative to +y axis; convert to +x-axis reference
  spirPA_rad = (spirPA + 90.0) * DEG2RAD;
    
  inc_rad = inclination * DEG2RAD;
  cosInc = cos(inc_rad);
  sinInc = sin(inc_rad);

  twosigma_squared = 2.0 * sigma*sigma;

  // We could do this here using integLimit as a class data member, but it tends
  // to be marginally slower this way
//  integLimit = INTEGRATION_MULTIPLIER * a_ring;
}


/* ---------------- PUBLIC METHOD: GetValue ---------------------------- */

double Spirals3DvarPitch::GetValue( double x, double y )
{
  double  x_diff = x - x0;
  double  y_diff = y - y0;
  double  xp, yp, x_d0, y_d0, z_d0, totalIntensity;
  double  integLimit;
  double  xyParameters[17];
  int  nSubsamples;
  
  // Calculate x,y in component's (projected sky) reference frame: xp,yp
  xp = x_diff*cosPA + y_diff*sinPA;
  yp = -x_diff*sinPA + y_diff*cosPA;

  // Calculate (x,y,z)_start in component's native xyz reference frame, corresponding to
  // intersection of line-of-sight ray with projected sky frame
  x_d0 = xp;
  y_d0 = yp * cosInc;
  z_d0 = yp * sinInc;

  // Set up parameter vector for the integration (everything that stays unchanged
  // for this particular xp,yp location)
  xyParameters[0] = x_d0;
  xyParameters[1] = y_d0;
  xyParameters[2] = z_d0;
  xyParameters[3] = cosInc;
  xyParameters[4] = sinInc;
  xyParameters[5] = cosPA;
  xyParameters[6] = sinPA;
  xyParameters[7] = q;
  xyParameters[8] = J_0;
  xyParameters[9] = pitch;
  xyParameters[10] = dPitch;
  xyParameters[11] = sigma;
  xyParameters[12] = sigmaVar;
  xyParameters[13] = h_z;
  xyParameters[14] = h;
  xyParameters[15] = twosigma_squared;
  xyParameters[16] = spirPA_rad;
  F.params = xyParameters;

  // integrate out to +/- integLimit, which is multiple of ring radius
  integLimit = INTEGRATION_MULTIPLIER * h;
  totalIntensity = Integrate(F, -integLimit, integLimit);

  return totalIntensity;
}







/* ----------------------------- OTHER FUNCTIONS -------------------------------- */


/* Compute luminosity density for a location (x_d,y_d,z_d) which is at line-of-sight 
 * distance s from start point (x_d0, y_d0, z_d0), where midplane of component (e.g.,
 * disk of galaxy) is oriented at angle (90 - inclination) to the line of sight vector. 
 */ 
double LuminosityDensitySpiralsVP( double s, void *params )
{

  double  y_d, z_d, R, deltaR, lumDensity, theta, e1, e2, e2arg, sech, verticalScaling, p_value, pitchR;
  double  x_spir, y_spir, z_spir, y_spir_scaled;
  double  *paramsVect = (double *)params;
  double  x_d0 = paramsVect[0];
  double  y_d0 = paramsVect[1];
  double  z_d0 = paramsVect[2];
  double  cosInc = paramsVect[3];
  double  sinInc = paramsVect[4];
  double  cosPA = paramsVect[5];
  double  sinPA = paramsVect[6];
  double  q = paramsVect[7];
  double  J_0 = paramsVect[8];
  double  pitch = paramsVect[9];
  double  dPitch = paramsVect[10];
  double  sigma = paramsVect[11];
  double  sigmaVar = paramsVect[12];
  double  h_z = paramsVect[13];
  double  h = paramsVect[14];
  double  twosigma_squared = paramsVect[15];
  double  spirPA_rad = paramsVect[16];
  
  // Given current value of s and the pre-defined parameters, determine our 
  // 3D location (x_d,y_d,z_d) [by construction, x_d = x_d0]
  y_d = y_d0 + s*sinInc;
  z_d = z_d0 - s*cosInc;
  
  // Convert 3D Cartesian coordinate to rotated x_ring,y_ring,z_ring coordinate,
  // where x_ring is along ring major axis
  x_spir = x_d0; //x_d0*cosPA + y_d*sinPA;
  y_spir = y_d; //-x_d0*sinPA + y_d*cosPA;
  z_spir = fabs(z_d);
  // NOTE: FOR A CONSTANT-WIDTH RING (i.e., where sigma does *not* scale with ellipticity),
  // we could: scale *a_ring* and compute R without scaling...
  // OR: proceed as normal, but rescale sigma to account for ellipticity...
  
  // Convert x_ring,y_ring to scaled radius R
  y_spir_scaled = y_spir/q;
  R = sqrt(x_spir*x_spir + y_spir_scaled*y_spir_scaled);
  theta = atan2(y_spir_scaled, x_spir);

  e1 = exp(-R/h);
  
  pitchR = pitch + dPitch*(R/h);
  p_value = 1.0/tan(DEG2RAD*pitchR);
  if (R>0.1*h)
    e2arg = (cos(theta - p_value*log(R)-spirPA_rad)) / (sigma/R+sigmaVar*R);
  else
    e2arg = 0.0;

  // if combination of n*z/z_0 is large enough, switch to simple exponential
  if ((z_spir/h_z) > COSH_LIMIT)
      verticalScaling = 2 * exp(-z_spir/h_z);
  else
      verticalScaling = 1.0 / cosh(z_spir/h_z);

  
  lumDensity = J_0 * e1 * exp(-(e2arg*e2arg)) * verticalScaling;
  //printf("%1.2e\n", verticalScaling);
  return lumDensity;
}



/* END OF FILE: func_gaussianring3d.cpp -------------------------------- */
