/*   Class interface definition for func_psf.cpp
 *   VERSION 0.3
 *
 *   A class derived from FunctionObject (function_object.h),
 * which generates the psf
 *
 * PARAMETERS:
 * x0 =  params[0 + offsetIndex];   -- center of component (pixels, x)
 * y0 =  params[1 + offsetIndex];   -- center of component (pixels, y)
 * I = params[2 + offsetIndex ];  -- total flux (ADU)
 *
 *
 */


// CLASS FPSF:

#include "function_object.h"

//#define CLASS_SHORT_NAME  "FPSF"


class FPSF : public FunctionObject
{
  // the following static constant will be defined/initialized in the .cpp file
  static const char  className[];

  public:
    // Constructors:
    FPSF( );
    // redefined method/member function:
    void  Setup( double params[], int offsetIndex, double xc, double yc );
    double  GetValue( double x, double y );
    // No destructor for now

    // class method for returning official short name of class
    static void GetClassShortName( string& classname ) { classname = className; };

  private:
  double  x0, y0, I;   // parameters
};
