/*   Class interface definition for func_sersic.cpp
 *   VERSION 0.3
 *
 *   A class derived from FunctionObject (function_object.h),
 * which produces the luminosity as a function of radius for an elliptical
 * Sersic.
 *
 * PARAMETERS:
 * x0 =   params[0 + offsetIndex];   -- center of component (pixels, x)
 * y0 =   params[1 + offsetIndex];   -- center of component (pixels, y)
 * ell =  params[2 + offsetIndex];   -- ellipticity
 * I_e =  params[3 + offsetIndex ];  -- half-light-radius intensity (ADU)
 * n =   params[4 + offsetIndex ];  -- Sersic index along the lobe
 * r_e = params[5 + offsetIndex ];  -- half-light radius (pixels) along the lobe
 * alpha = params[6 + offsetIndex]; -- sharpness of the transaction
 * beta = params[7 + offsetIndex]; -- outer slope
 * gamma = params[8 + offsetIndex]; -- inner slope
 * r_b = params[9 + offsetIndex];
 * asymm = params[10 + offsetIndex ]; -- asymmetry between x and y distances
 */


// CLASS Sersic:

#include "function_object.h"

//#define CLASS_SHORT_NAME  "Sersic"


class Sharpx : public FunctionObject
{
  // the following static constant will be defined/initialized in the .cpp file
  static const char  className[];

  public:
    // Constructors:
    Sharpx( );
    // redefined method/member function:
    void  Setup( double params[], int offsetIndex, double xc, double yc );
    double  GetValue( double x, double y );
    // No destructor for now

    // class method for returning official short name of class
    static void GetClassShortName( string& classname ) { classname = className; };


  private:
    double  x0, y0, PA, ell, I_e, n, r_e, alpha, beta, gamma, r_b, asymm;   // parameters
    double  nSquared, bn, invn, powerFraction;
  double  q;   // other useful (shape-related) quantities
};
