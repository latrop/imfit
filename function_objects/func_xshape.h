/*   Class interface definition for func_sersic.cpp
 *   VERSION 0.3
 *
 *   A class derived from FunctionObject (function_object.h),
 * which produces the luminosity as a function of radius for an elliptical
 * Sersic.
 *
 * PARAMETERS:
 * x0 =  params[0 + offsetIndex];   -- center of component (pixels, x)
 * y0 =  params[1 + offsetIndex];   -- center of component (pixels, y)
 * PA =  params[2 + offsetIndex];   -- PA of component, rel. to +x axis
 * ell = params[3 + offsetIndex];   -- ellipticity
 * I0 = params[4 + offsetIndex];   -- cental brightness
 * rout = params[5 + offsetIndex];   -- Ferrer out truncation radius
 * alpha = params[6 + offsetIndex];   -- Ferrer alpha parameter
 * beta = params[7 + offsetIndex];   -- Ferrer beta parameter
 * z0 = params[8 + offsetIndex];   -- Vertical scalelength
 * ax = params[9 + offsenIndex];   -- horisonthal Fourier modification
 * ay = params[10 + offsenIndex];   -- vertical Fourier modification
 */


// CLASS Xshape:

#include "function_object.h"

#define CLASS_SHORT_NAME  "Xshape"


class Xshape : public FunctionObject
{
  // the following static constant will be defined/initialized in the .cpp file
  static const char  className[];

  public:
    // Constructors:
    Xshape( );
    // redefined method/member function:
    void  Setup( double params[], int offsetIndex, double xc, double yc );
    double  GetValue( double x, double y );
    // No destructor for now

    // class method for returning official short name of class
    static void GetClassShortName( string& classname ) { classname = className; };


  protected:
    double CalculateIntensity( double R, double z );
    int  CalculateSubsamples( double R, double z );


  private:
    double  x0, y0, PA, ell, I0, rout, alpha, beta, z0, ax, ay;   // parameters
  double  n2, bn, invn;
  double  q, PA_rad, cosPA, sinPA;   // other useful (shape-related) quantities
};
