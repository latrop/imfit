/* FILE: func_sharpx.cpp ----------------------------------------------- */
/* VERSION 0.3
 *
 *   Function object class for a Sersic function, with constant
 * ellipticity and position angle (pure elliptical, not generalized).
 *   
 *   BASIC IDEA:
 *      Setup() is called as the first part of invoking the function;
 *      it pre-computes various things that don't depend on x and y.
 *      GetValue() then completes the calculation, using the actual value
 *      of x and y, and returns the result.
 *      So for an image, we expect the user to call Setup() once at
 *      the start, then loop through the pixels of the image, calling
 *      GetValue() to compute the function results for each pixel coordinate
 *      (x,y).
 *
 *   NOTE: Currently, we assume input PA is in *degrees* [and then we
 * convert it to radians] relative to +x axis.
 *
 *   MODIFICATION HISTORY:
 *     [v0.4]  20--26 Mar 2010: Preliminary support for pixel subsampling.
 *     [v0.3]: 21 Jan 2010: Modified to treat x0,y0 as separate inputs.
 *     [v0.2]: 28 Nov 2009: Updated to new FunctionObject interface.
 *     [v0.1]: 19 Nov 2009: Created (as modification of func_exp.cpp.
 */

// Copyright 2010, 2011, 2012, 2013 by Peter Erwin.
// 
// This file is part of Imfit.
// 
// Imfit is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your
// option) any later version.
// 
// Imfit is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
// 
// You should have received a copy of the GNU General Public License along
// with Imfit.  If not, see <http://www.gnu.org/licenses/>.



/* ------------------------ Include Files (Header Files )--------------- */
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <string>
#include <algorithm>

#include "func_sharpx.h"

using namespace std;


/* ---------------- Definitions ---------------------------------------- */
const int  N_PARAMS = 8;
const char  PARAM_LABELS[][20] = {"ell", "I_e", "n1", "r_e1", "n2", "r_e2", "asymm", "bend"};
const char  FUNCTION_NAME[] = "Sharpx function";
//const char SHORT_FUNCTION_NAME[] = "Sharpx";
const double  DEG2RAD = 0.017453292519943295;
const int  SUBSAMPLE_R = 10;

const char Sharpx::className[] = "Sharpx";

const double  A0_M03 = 0.01945;
const double  A1_M03 = -0.8902;
const double  A2_M03 = 10.95;
const double  A3_M03 = -19.67;
const double  A4_M03 = 13.43;


/* ---------------- CONSTRUCTOR ---------------------------------------- */

Sharpx::Sharpx( )
{
  string  paramName;
  
  nParams = N_PARAMS;
  functionName = FUNCTION_NAME;
  shortFunctionName = className;
  // Set up the vector of parameter labels
  for (int i = 0; i < nParams; i++) {
    paramName = PARAM_LABELS[i];
    parameterLabels.push_back(paramName);
  }
  
  doSubsampling = true;
}


/* ---------------- PUBLIC METHOD: Setup ------------------------------- */

void Sharpx::Setup( double params[], int offsetIndex, double xc, double yc )
{
  x0 = xc;
  y0 = yc;
  ell = params[0 + offsetIndex ];
  I_e =  params[1 + offsetIndex ];
  n1 =   params[2 + offsetIndex ];
  r_e1 = params[3 + offsetIndex ];
  n2 =   params[4 + offsetIndex ];
  r_e2 = params[5 + offsetIndex ];
  asymm = params[6 + offsetIndex ];
  bend = params[7 + offsetIndex ];

  // pre-compute useful things for this round of invoking the function
  q = 1.0 - ell;
  // convert PA to +x-axis reference and then to radians
  n1Squared = n1 * n1;
  if (n1 > 0.36){
    bn1 = 2*n1 - 0.333333333333333 + 0.009876543209876543/n1
         + 0.0018028610621203215/n1Squared + 0.00011409410586365319/(n1Squared*n1)
         - 7.1510122958919723e-05/(n1Squared*n1Squared);
  }
   else {
    bn1 = A0_M03 + A1_M03*n1 + A2_M03*n1Squared + A3_M03*n1Squared*n1 + A4_M03*n1Squared*n1Squared;
  }
  invn1 = 1.0 / n1;

  n2Squared = n2 * n2;
  if (n2 > 0.36){
    bn2 = 2*n2 - 0.333333333333333 + 0.009876543209876543/n2
         + 0.0018028610621203215/n2Squared + 0.00011409410586365319/(n2Squared*n2)
         - 7.1510122958919723e-05/(n2Squared*n2Squared);
  }
   else {
    bn2 = A0_M03 + A1_M03*n2 + A2_M03*n2Squared + A3_M03*n2Squared*n2 + A4_M03*n2Squared*n2Squared;
  }
  invn2 = 1.0 / n2;
}


/* ---------------- PRIVATE METHOD: CalculateIntensity ----------------- */
// This function calculates the intensity for a Sersic function at radius r,
// with the various parameters and derived values (n, b_n, r_e, etc.)
// pre-calculated by Setup().

/* ---------------- PUBLIC METHOD: GetValue ---------------------------- */
// This function calculates and returns the intensity value for a pixel with
// coordinates (x,y), including pixel subsampling if necessary (and if subsampling
// is turned on). The CalculateIntensity() function is called for the actual
// intensity calculation.

double Sharpx::GetValue( double x, double y )
{
  double  x_diff = x - x0;
  double  y_diff = y - y0;
  double  xp, yp_scaled, r, totalIntensity;
  double lobeDist, xOnLobe, yOnLobe, centerDist, factorAlongLobe, factorAcrossLobe, c, k;
  int  nSubsamples;
  
  // Calculate x,y in component reference frame, and scale y by 1/axis_ratio
  xp = x_diff;
  yp_scaled = y_diff/q;

  // Bending mode
  if (yp_scaled > 0)
    yp_scaled = yp_scaled + pow(bend*(xp / r_e1), 2.0);
  else
    yp_scaled = yp_scaled - pow(bend*(xp / r_e1), 2.0);

  if ((x_diff > 0) && (y_diff > 0)) {
    // The first quadrant
    k = 1.0;
  }
  if ((x_diff < 0) && (y_diff > 0)) {
    // The second quadrant
    k = -1.0;
  }
  if ((x_diff < 0) && (y_diff < 0)) {
    // The third quadrant
    k = 1.0;
  }
  if ((x_diff > 0) && (y_diff < 0)) {
    // The fourth quadrant
    k = -1.0;
  }
  

  // Calculate the distance from the point (x,y) to the lobe center
  lobeDist = 0.707106 * abs(k*xp - yp_scaled);
  // Calculate the coodinates of the nearest point on the lobe:

  xOnLobe = 0;
  yOnLobe = xOnLobe / q;    
  if ((x_diff >= 0) && (y_diff >= 0)) {
    // The first quadrant
    if (xp >= yp_scaled){
      xOnLobe = xp - 0.707106*lobeDist;
      lobeDist = lobeDist / asymm;}
    else
      xOnLobe = xp + 0.707106*lobeDist;
  }
  if ((x_diff < 0) && (y_diff > 0)) {
    // The second quadrant
    if (abs(xp) >= abs(yp_scaled)){
      xOnLobe = xp + 0.707106*lobeDist;
    lobeDist = lobeDist / asymm;}
    else 
      xOnLobe = xp - 0.707106*lobeDist;
  }
  if ((x_diff < 0) && (y_diff <= 0)) {
    // The third quadrant
    if (abs(xp) >= abs(yp_scaled)){
      xOnLobe = xp + 0.707106*lobeDist;
      lobeDist = lobeDist / asymm;}
    else 
      xOnLobe = xp - 0.707106*lobeDist;
  }
  if ((x_diff >= 0) && (y_diff < 0)) {
    // The fourth quadrant
    if (abs(xp) >= abs(yp_scaled)){
      xOnLobe = xp - 0.707106*lobeDist;
      lobeDist = lobeDist / asymm;}
    else
      xOnLobe = xp + 0.707106*lobeDist;
  }
 
  yOnLobe = xOnLobe / q;

  

  // Calculate the distance from the point on lobe to the galaxy center
  centerDist = sqrt(xOnLobe*xOnLobe + yOnLobe*yOnLobe);


  factorAlongLobe = exp( -bn1 * (pow((centerDist/r_e1), invn1) - 1.0));
  factorAcrossLobe = exp( -bn2 * (pow((lobeDist/r_e2), invn2) - 1.0));

  totalIntensity = I_e * factorAlongLobe * factorAcrossLobe;

  return totalIntensity;
}

/* END OF FILE: func_sersic.cpp ---------------------------------------- */
